$(document).ready(function () { //Attendre que le DOM soit chargé

   let ArrayCartesInitiales = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]; //création d'un array avec i allant de 0 a 13 i++
   let ArrayCartesMelangees = melangerCartes(ArrayCartesInitiales); // on melange le tableau initiale pour melanger les cartes
   creerPlateauJeu(); // on creer le plateau en appellant la focntion creerPlateauJeu

   let compteur = 0; // limiter le nombre de clic a 2 pour qu'ils ne puissent pas retourner plus de 2 cartes
   $("#idConteneurJeu").on("click", "div", function (e) { // click sur le boutton "Verifier Paire"

      if (compteur < 2) { //Si le compteur est inferieur a 2
         compteur = compteur + 1; // incrementation du compteur de click
         let NbPaires = 0; //Initialisation d'un varaible de comptage de paire
         let indCarteCliquee = $(this).data("id"); //on recupère l'id de la carte qui vient d'etre retournée et on la stock dans une variable
         indCarteCliquee = Number(indCarteCliquee); //on change la valeur en nombre (string vers nombre)
         $(this).removeClass("carteDos"); //on change ca class afin de changer l'img et la "retourner"
         $(this).addClass(ArrayCartesMelangees[indCarteCliquee].img); // on lui donne l'img qui est definie dans le tableau des cartes melangées a l'index de son id 
         ArrayCartesMelangees[indCarteCliquee].trouvee = true; // son attribut trouvee devient donc true puisqu'elle est retournée

         $("#BtnRecuppaire").on("click", function () { // lorsqu'on click sur le boutton  de verification de la paire 
            NbPaires = 0; // le nombre de paire est remis a zero
            for (let i = 0; i < 7; i++) { //pour i allant de 0 a 6
               if ((ArrayCartesMelangees[i].trouvee == true) && (ArrayCartesMelangees[i + 7].trouvee == true)) { // si la carte d'index i ET la carted'index i + 7 (sa paire) ont la valeur true dans l'attribut trouvee
                  $(".img" + i).attr("class", "carteTrouvee"); // alors retournée la carte d'index i et changer son image en carte trouvée
                  $(".img" + (i + 7)).attr("class", "carteTrouvee"); // et retournée sa carte paire qui a aussi était retournée
                  NbPaires = NbPaires + 1; //nombre de paire + 1
               }
               else if ((ArrayCartesMelangees[i].trouvee == true) && (ArrayCartesMelangees[i + 7].trouvee == false)) { //sinon, si la carte d'index i et retournée mais pas sa paire
                  $(".img" + i).attr("class", "carteDos"); //retournée la carte d'index  i et son image en carte Dos
                  ArrayCartesMelangees[i].trouvee = false; //et la valeur de son attribut trouvee devient false puisque nous l'avons retourné
               }
               else if ((ArrayCartesMelangees[i].trouvee == false) && (ArrayCartesMelangees[i + 7].trouvee == true)) {//sinon, si la carte d'index i n'est pas retournée mais sa paire si,
                  $(".img" + (i + 7)).attr("class", "carteDos"); //retournée sa paire
                  ArrayCartesMelangees[(i + 7)].trouvee = false; // changer sa valeur d'attribut trouvee en false
               }
               else if ((ArrayCartesMelangees[i].trouvee == false) && (ArrayCartesMelangees[i + 7].trouvee == false)) {//sinon, si aucunes des deux n'est retournée
                  $(".img" + i).attr("class", "carteDos"); //garder la carte retournée
                  ArrayCartesMelangees[i].trouvee = false; //garder sa valeur d'attribut trouvée en false
                  $(".img" + (i + 7)).attr("class", "carteDos"); // pareille pour sa paire
                  ArrayCartesMelangees[(i + 7)].trouvee = false;
               }
               

               $("#idNbPairesTrouvées").html(NbPaires); //affichage du nombre de paire tourvées
               if (NbPaires == 7) { //Si le nombre de apire est egal a 7
                  alert("Félicitation"); //afficher un message d'alerte "Felicitation"
                  for (let i = 0; i < 14; i++) { //et pour i allant de 0 a 14
                     $(".img" + i).attr("class", "carteDos"); //retourner les cartes
                     ArrayCartesMelangees[i].trouvee = false; //changer leur valeur d'attribut trouvee en false
                  }
                  window.location.reload(); //refresh la page pour refaire une partie avec les cartes melangées
               }
               compteur = 0; // remettre le compteur a zero pour retrournée une autre paire
            }
         })
      }


   });











   function creerPlateauJeu() { //fonction "creerPlateauJeu" sans parametres
      for (let i = 0; i < 14; i++) { //boucle de creation des div avec l'id de l'index des cartes melangées
         $("#idConteneurJeu").append('<div class="carteDos" data-id="' + ArrayCartesMelangees[i] + '"></div>'); //creer une div apres la derniere crée avec l'id de l'index du tableau des cartes melangées
      }; //fin de fonction , ne retourne rien
   }


   function melangerCartes(prmarray) { //fonction "melangerCartes" avec un tableau en parametre
      let nouvellePosition;   //variable de stockage de position temporelle afin de l'echanger
      let positionchangement; //

      for (let i = prmarray.length - 1; i > 0; i--) { //boucle qui melange les cartes en melangeant les lignes du tableau
         nouvellePosition = Math.floor(Math.random() * (i + 1)); //pour savoir avec quelle carte changer la carte d'index i on prend la partie entiere d'un nombre random
         positionchangement = prmarray[i]; //elle garde la valeur de la ligne du tableau d'un l'index est le random pour vider la ligne afin d'y mettre celle d'index i
         prmarray[i] = prmarray[nouvellePosition]; // on met notre carte dedans
         prmarray[nouvellePosition] = positionchangement; // et on met ce qu'il y avait avant dans l'ancienne ligne d'index i
      }
      return prmarray; //elle retourne un tableau de lignes melangées
   };


   ArrayCartesMelangees = []; //tableau avec les attribut img et trouvee
   ArrayCartesMelangees.push({ img: "img0", trouvee: false });
   ArrayCartesMelangees.push({ img: "img1", trouvee: false });
   ArrayCartesMelangees.push({ img: "img2", trouvee: false });
   ArrayCartesMelangees.push({ img: "img3", trouvee: false });
   ArrayCartesMelangees.push({ img: "img4", trouvee: false });
   ArrayCartesMelangees.push({ img: "img5", trouvee: false });
   ArrayCartesMelangees.push({ img: "img6", trouvee: false });
   ArrayCartesMelangees.push({ img: "img0", trouvee: false });
   ArrayCartesMelangees.push({ img: "img1", trouvee: false });
   ArrayCartesMelangees.push({ img: "img2", trouvee: false });
   ArrayCartesMelangees.push({ img: "img3", trouvee: false });
   ArrayCartesMelangees.push({ img: "img4", trouvee: false });
   ArrayCartesMelangees.push({ img: "img5", trouvee: false });
   ArrayCartesMelangees.push({ img: "img6", trouvee: false });


});
