/*
BRENNE Phoebé
Créer le 30 04 2020
Version 2.0
Fichier imc.js 
fonctionne avec index.html du dossier JS3imcVersion1 et style.css

Prend les valeurs entrées dans les cases, les tranformes en number, verifie que ce soit des nombres, 
remplace les virgules par des point et calcul l'imc qui est renvoyé 

Fonctionne avec la fonction calculeriMC

*/
/*Attend que les document soit chargé */
$(document).ready(function () {
    /*Lorsque l'on appuie sur le boutton "Calculer L'imc" */
    $("#btnCalculImc").click(function () {

        let messageerror = "";               // variable de message d'erreur
        let poids = $("#idPoids").val();    //variable qui recupere ce qu'il y a d'ecris dans la case poids a l'id idPoids
        let taille = $("#idTaille").val();  //variable qui recupere ce qu'il y a d'ecris dans la case taille a l'id idTaille

        poids = poids.replace(",", ".");    //remplace les virgule par des points de la case Poids pour eviter les erreurs
        taille = taille.replace(",", ".");  //remplace les virgule par des points de la case Taille pour eviter les erreurs

        poids = Number(poids);      //Tansforme le contenu en Number 
        taille = Number(taille);    //Tansforme le contenu en Number

        if (isNaN(poids) == true || isNaN(taille) == true) {    //si la case taille ou la case poids contient une valeur qui n'est pas un caractère numerique
            messageerror = "Veuillez verifier que les caractères entrés sont des caractères numeriques"; //créer une alert qui leur demande de changer leur valeures
            alert(messageerror);    //Affiche le message d'erreur

        } else {        //Sinon calculer l'imc

            let imc = 0; //variable allant contenir le resultat de la fonction calculerimc
            let imctofixed = 0; //variable allant contenir le resultat de l'imc avec un chiffre apres la virgule
            let interpret ="";                  //variable allant contenir le resultat de l' interpretation de l'imc
            imc = calculerIMC(poids, taille);  //appelle la fonction calculerimc
            imctofixed = imc.toFixed(1);        //fixe la virgule a un chiffre apres la virgule
            $("#textIMC").html(imctofixed);     //modifie le texte afin d'afficher l'imc
            interpret = interpreterIMC(imctofixed); //appelle la fonction interpretation de l'imc
            $("#interpretIMC").html(interpret);      //Affichage sur l'index par le resultat de l'interpretation
        }

    });



    function calculerIMC(prmPoids, prmTaille) {         //fonction calculerim avec 2 parametre taille et poids
        let valRetour = prmPoids / (prmTaille * prmTaille);  //calcul l'imc 
        return valRetour;                               //renvoie la valeur de l'imc
    }                                                   //fin de fonction

});

    function interpreterIMC(prmIMC){                //fonction interpreterIMC
        let interpretation = ""                     //variable allant contenir le resultat de la fonction interpreterIMC
        
        if(prmIMC < 16.5){                          //Tout les cas ou l'on compare l'Interpretation, lorsqu'elle vaut ... si l'imc est egal a ...
            interpretation = "Dénutrition";            
        }
        
        else if((prmIMC >= 16.5 )&&(prmIMC < 18.5)){
            interpretation = "Maigreur";            
        }

        else if((prmIMC >= 18.5 )&&(prmIMC < 25)){
            interpretation = "Corpulence Normale";
        }

        else if((prmIMC >= 25 )&&(prmIMC < 30)){
            interpretation = "Surpoids";
        }

        else if((prmIMC >= 30 )&&(prmIMC < 35)){
            interpretation = "Obésité modérée";
        }

        else if((prmIMC >= 35 )&&(prmIMC < 40)){
            interpretation = "Obésité sévère";
        }

        else if(prmIMC >= 40 ){
            interpretation = "Obésité morbide";
        }
        return interpretation;                  //Envoie le resultat de l'interpratation de l'imc par la fonction

    }                                           //fin de fonction