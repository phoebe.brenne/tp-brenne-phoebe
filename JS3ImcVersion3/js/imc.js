/*
BRENNE Phoebé
Créer le 30 04 2020
Version 3.0
Fichier imc.js 
fonctionne avec index.html du dossier JS3imcVersion1 et style.css

Prend les valeurs entrées dans les cases, les tranformes en number, verifie que ce soit des nombres, 
remplace les virgules par des point et calcul l'imc qui est renvoyé 

Fonctionne avec la fonction calculeriMC

*/
/*Attend que les document soit chargé */
$(document).ready(function () {



    $("#idSliderPoids").on('input', function () {       //evenement lorsqu'on change le curseur de poids de place
        let poids = $("#idSliderPoids").val();    //variable qui recupere ce qu'il y a d'ecris dans la case poids a l'id idPoids
        let taille = $("#idSliderTaille").val();  //variable qui recupere ce qu'il y a d'ecris dans la case taille a l'id idTaille
        $("#textPoids").html(poids);        //affichage a coté du curseru de poids la valeur correspondante
        $("#textTaille").html(taille);      //affichage a coté du curseru de taille la valeur correspondante

        poids = Number(poids);      //Tansforme le contenu en Number 
        taille = Number(taille);    //Tansforme le contenu en Number

        let imc ; //variable allant contenir le resultat de la fonction calculerimc
        let imctofixed ; //variable allant contenir le resultat de l'imc avec un chiffre apres la virgule
        imc = calculerIMC(poids, taille);  //appelle la fonction calculerimc
        imctofixed = imc.toFixed(1);        //fixe la virgule a un chiffre apres la virgule
        let interIMC = interpreterIMC(imctofixed);  //appelle de la fonction interpreterIMC
        $("#textIMC").html(imctofixed + " " + interIMC);     //modifie le texte afin d'afficher l'imc

    });


    $("#idSliderTaille").on('input', function () {      //evenement lorsqu'on change le curseur de taille de place
        let poids = $("#idSliderPoids").val();    //variable qui recupere ce qu'il y a d'ecris dans la case poids a l'id idPoids
        let taille = $("#idSliderTaille").val();  //variable qui recupere ce qu'il y a d'ecris dans la case taille a l'id idTaille
        $("#textPoids").html(poids);        //affichage a coté du curseru de poids la valeur correspondante
        $("#textTaille").html(taille);      //affichage a coté du curseru de taille la valeur correspondante


        poids = Number(poids);      //Tansforme le contenu en Number 
        taille = Number(taille);    //Tansforme le contenu en Number



        let imc = 0; //variable allant contenir le resultat de la fonction calculerimc
        let imctofixed = 0; //variable allant contenir le resultat de l'imc avec un chiffre apres la virgule
        imc = calculerIMC(poids, taille);  //appelle la fonction calculerimc
        imctofixed = imc.toFixed(1);        //fixe la virgule a un chiffre apres la virgule
        let interIMC = interpreterIMC(imctofixed);  //appelle de la fonction interpreterIMC
        $("#textIMC").html(imctofixed + " " + interIMC);     //modifie le texte afin d'afficher l'imc

    });





    function calculerIMC(prmPoids, prmTaille) {         //fonction calculerim avec 2 parametre taille et poids
        prmTaille = prmTaille /100 ;                        //remettre en m afin de calculer l'imc
        let valRetour = prmPoids / (prmTaille * prmTaille);  //calcul l'imc 
        return valRetour;                               //renvoie la valeur de l'imc
    }                                                   //fin de fonction



    function interpreterIMC(prmIMC) { //fnonction qui interprete k'imc et en retourne l'etat de "santé"
        let resultinter = "";

        if (prmIMC < 16.5) {
            resultinter = "Dénutrition";
        }
        else if ((prmIMC >= 16.5) && (prmIMC < 18.5)) {
            resultinter = "Maigreur";
        }

        else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
            resultinter = "Corpulence Normale";
        }

        else if ((prmIMC >= 25) && (prmIMC < 30)) {
            resultinter = "Surpoids";
        }
        else if ((prmIMC >= 30) && (prmIMC < 35)) {
            resultinter = "Obesité Modéré";
        }

        else if ((prmIMC >= 35) && (prmIMC < 40)) {
            resultinter = "Obesité Sévère";
        }

        else if (prmIMC >= 40) {
            resultinter = "Obesité Morbide";
        }

        return resultinter ;
    }

});

