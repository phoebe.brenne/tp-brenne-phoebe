/*
BRENNE Phoebé
le 31/03/2020
version 1

Fichier sometab.js qui ajout une case avec la valeur du nombre de cases du tableau
et qui supprime la derniere valeur du tableau en fonction des clics sur les boutons (input)
du fichier index.html.
Necessite Jquery et un fichier css (tab.css)
*/



$(document).ready(function () {     //attend que le document soit chargé



    $("#ajout").click(              //evenement clique sur le bouton Ajouter
        function () {
            let longueurtableau = 1 + $("table tbody td").length;   //calcule la longueur du tableau +1 pour demarrer a 1
            let sommerecu = calculTotal(longueurtableau); //Appelle de la fonction qui calcul la somme

            $("table tbody").append("<tr><td>" + longueurtableau + "</td></tr>"); //ajout d'une case avec le nombre de ligne du tableauen fin de tableau
            $("table tfoot tr").html("<td>" + sommerecu + "</td>"); //modification de la somme en bas de tableau
        }
    );



    $("#supp").click(      //evenement clique sur le bouton Supprimer
        function () {
            $("table tbody tr td:last").remove();   //effacement de la derniere ligne du tableau
            let longueurtableau = $("table tbody td").length; //calcul de la longueur du tableau pas de +1 pour ne pas faire de decalage a cause du symbole (<=) dans la fonction de somme
            let sommerecu = calculTotal(longueurtableau); //appelle de la fonction de calcul de la somme

            $("table tfoot tr").replaceWith("<tr><td>" + sommerecu + "</td></tr>"); //remplacement de la valeur de la somme
        }
    );



    function calculTotal(prmlongueur) { //Fonction de calcul de la somme
        let somme = 0;                  //initialisation d'une valeur de retour
        for (let i = 0; i <= prmlongueur; i++) {    //boucle de calcul de somme a chaque pas tant que i <= a la longueur du tableau
            somme = somme + i;
        }
        return somme; //retour de valeur
    }


});

