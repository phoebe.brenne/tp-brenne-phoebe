/*
BRENNE Phoebé
Crée le 26/03
Version 1.0

Application qui renvois les 17 premiers nombres de la suite de Fibonnaci

*/

let msg = Fibo();
alert(msg);





function Fibo() {                        //Fonction retournant la suite des 17 premiers nombre de Fibonnaci

  let maxVect = 17;                      //taille du vect et maximal de la boucle
  let msgretour = "La suite de Fibonnaci commence par : \n 0 \n 1 \n ";
  let vectFibo = [0, 1];     //Initialisation du vecteur
  let resultatFibo = 0;                //Initialisation du resultat
  let Fibo1 = 0;                       //Initialisation d'une des operandes du resultat
  let Fibo2 = 0;                       //Initialisation d'une des operandes du resultat

  for (let i = 2; i < maxVect; i++) {   //boucle de calcul du prochain nombre de Fibonnaci
    Fibo1 = vectFibo[i - 1];                //calcul de i-1 pour les operandes
    Fibo2 = vectFibo[i - 2];                //calcul de i-2 pour les operandes
    resultatFibo = Fibo1 + Fibo2;         //calcul du nombre de Fibo
    vectFibo[i] = resultatFibo;           //Inserstion dans le vect

    msgretour = msgretour + resultatFibo + "\n";  //création du message de retour

  }                                 //Fin boucle

  return msgretour;
}

