/*
BRENNE Phoebé
Créer le 30 04 2020
Version 6.0
Fichier imc.js 
fonctionne avec index.html du dossier JS3imcVersion1 et style.css

Contient  deux curseurs afin de  changer la valeur du poids et de la taille, en calcul d'IMC, le renvoie et en affiche " l'etat de santé "
sur une balance colorée avec aiguille qui se deplace en fonction de la valeur de l'IMC, elle affiche une silhouette du sexe choisie. 



*/
/*Attend que les document soit chargé */
$(document).ready(function () {



    $("#idSliderPoids").on('input', function () {       //evenement lorsqu'on change le curseur de poids de place
        let poids = $("#idSliderPoids").val();    //variable qui recupere ce qu'il y a d'ecris dans la case poids a l'id idPoids
        let taille = $("#idSliderTaille").val();  //variable qui recupere ce qu'il y a d'ecris dans la case taille a l'id idTaille
        $("#textPoids").html(poids);        //affichage a coté du curseru de poids la valeur correspondante
        $("#textTaille").html(taille);      //affichage a coté du curseru de taille la valeur correspondante

        poids = Number(poids);      //Tansforme le contenu en Number 
        taille = Number(taille);    //Tansforme le contenu en Number

        let imc ; //variable allant contenir le resultat de la fonction calculerimc
        let imctofixed ; //variable allant contenir le resultat de l'imc avec un chiffre apres la virgule
        imc = calculerIMC(poids, taille);  //appelle la fonction calculerimc
        imctofixed = imc.toFixed(1);        //fixe la virgule a un chiffre apres la virgule
        let interIMC = interpreterIMC(imctofixed);  //appelle de la fonction interpreterIMC
        $("#textIMC").html(imctofixed + " " + interIMC);     //modifie le texte afin d'afficher l'imc
        afficherBalance(imctofixed) ; //appelle de la fonction d'affichage de l'aiguille sr la balance
        afficherSilhouette(imctofixed) ; //appelle de la fonction d'affichage de la silhouette

    });


    $("#idSliderTaille").on('input', function () {      //evenement lorsqu'on change le curseur de taille de place
        let poids = $("#idSliderPoids").val();    //variable qui recupere ce qu'il y a d'ecris dans la case poids a l'id idPoids
        let taille = $("#idSliderTaille").val();  //variable qui recupere ce qu'il y a d'ecris dans la case taille a l'id idTaille
        $("#textPoids").html(poids);        //affichage a coté du curseru de poids la valeur correspondante
        $("#textTaille").html(taille);      //affichage a coté du curseru de taille la valeur correspondante


        poids = Number(poids);      //Tansforme le contenu en Number 
        taille = Number(taille);    //Tansforme le contenu en Number



        let imc = 0; //variable allant contenir le resultat de la fonction calculerimc
        let imctofixed = 0; //variable allant contenir le resultat de l'imc avec un chiffre apres la virgule
        imc = calculerIMC(poids, taille);  //appelle la fonction calculerimc
        imctofixed = imc.toFixed(1);        //fixe la virgule a un chiffre apres la virgule
        let interIMC = interpreterIMC(imctofixed);  //appelle de la fonction interpreterIMC
        $("#textIMC").html(imctofixed + " " + interIMC);     //modifie le texte afin d'afficher l'imc
        afficherBalance(imctofixed) ;  //appelle de la fonction d'affichage de l'aiguille sr la balance
        afficherSilhouette(imctofixed) ;  //appelle de la fonction d'affichage de la silhouette

    });


    $("input[name=sexe").on('change', function(){       //eventment lorsqu'un sexe est choisi
        
        let sexe = $("input[name=sexe]:checked").val(); //copie la valeur de la case cochée et la recopie dans la variable sexe

        if(sexe == "Homme"){    //en fonction de la valeur de la variable sexe, il change ou non la silhouette d'homme ou de femme
            $("#idSilhouette").css("background-image", "url(css/img/IMC-homme.jpg)");
        }else{
            
            $("#idSilhouette").css("background-image", "url(css/img/IMC-femme.jpg)");
        }

        let imc = 0; //variable allant contenir le resultat de la fonction calculerimc
        let imctofixed = 0; //variable allant contenir le resultat de l'imc avec un chiffre apres la virgule
        imc = calculerIMC(poids, taille);  //appelle la fonction calculerimc
        imctofixed = imc.toFixed(1);        //fixe la virgule a un chiffre apres la virgule
        let interIMC = interpreterIMC(imctofixed);  //appelle de la fonction interpreterIMC
        $("#textIMC").html(imctofixed + " " + interIMC);     //modifie le texte afin d'afficher l'imc
        afficherBalance(imctofixed) ;  //appelle de la fonction d'affichage de l'aiguille sr la balance
        afficherSilhouette(imctofixed) ;  //appelle de la fonction d'affichage de la silhouette
        
    });








    function calculerIMC(prmPoids, prmTaille) {         //fonction calculerim avec 2 parametre taille et poids
        prmTaille = prmTaille /100 ;                        //remettre en m afin de calculer l'imc
        let valRetour = prmPoids / (prmTaille * prmTaille);  //calcul l'imc 
        return valRetour;                               //renvoie la valeur de l'imc
    }                                                   //fin de fonction



    function interpreterIMC(prmIMC) { //fnonction qui interprete k'imc et en retourne l'etat de "santé"
        let resultinter = "";

        if (prmIMC < 16.5) {
            resultinter = "Dénutrition";
        }
        else if ((prmIMC >= 16.5) && (prmIMC < 18.5)) {
            resultinter = "Maigreur";
        }

        else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
            resultinter = "Corpulence Normale";
        }

        else if ((prmIMC >= 25) && (prmIMC < 30)) {
            resultinter = "Surpoids";
        }
        else if ((prmIMC >= 30) && (prmIMC < 35)) {
            resultinter = "Obesité Modéré";
        }

        else if ((prmIMC >= 35) && (prmIMC < 40)) {
            resultinter = "Obesité Sévère";
        }

        else if (prmIMC > 40) {
            resultinter = "Obesité Morbide";
        }

        return resultinter ;
    }




    function afficherBalance(prmIMC){           //fonction qui change l'aiguille en fonction de l'IMC
        let deplacement = 5 + (6.15 * prmIMC - 61.5) ;        //calcul de deplacement de l'aiguille par rapport a l'IMC
        $("#idAiguille").css("left", deplacement + "px") ;  //Application du changement de place de l'aiguille
    }


    function afficherSilhouette(prmIMC){    //fonction d'afficahge de la silhouette en fonction de l'imc
        let categorieIMC = 0 ;              //calcul du décalage de la silhouette en fonction de la categorie de l'imc
        
        if (prmIMC < 18.5) {
            categorieIMC = 0;   
        }

        else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
            categorieIMC = -105;
        }

        else if ((prmIMC >= 25) && (prmIMC < 30)) {
            categorieIMC = -210;
        }
        else if ((prmIMC >= 30) && (prmIMC < 35)) {
            categorieIMC = -315;
        }

        else if ((prmIMC >= 35) && (prmIMC < 40)) {
            categorieIMC = -420;
        }

        else if (prmIMC > 40) {
            categorieIMC = -525;
        }    
        
        
        $("#idSilhouette").css("background-position", categorieIMC); //decalage de la silhouette en fonction de l'imc

    }

});

