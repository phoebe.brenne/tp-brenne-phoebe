/*
BRENNE Phoebé
Créer le 30 04 2020
Fichier imc.js 
fonctionne avec index.html du dossier JS3imcVersion1 et style.css

Prend les valeurs entrées dans les cases, les tranformes en number, verifie que ce soit des nombres, 
remplace les virgules par des point et calcul l'imc qui est renvoyé 

Fonctionne avec la fonction calculeriMC

*/
/*Attend que les document soit chargé */
$(document).ready(function () {
    /*Lorsque l'on appuie sur le boutton "Calculer L'imc" */
    $("#btnCalculImc").click(function () {

        let messageerror = "";               // variable de message d'erreur
        let poids = $("#idPoids").val();    //variable qui recupere ce qu'il y a d'ecris dans la case poids a l'id idPoids
        let taille = $("#idTaille").val();  //variable qui recupere ce qu'il y a d'ecris dans la case taille a l'id idTaille

        poids = poids.replace(",", ".");    //remplace les virgule par des points de la case Poids pour eviter les erreurs
        taille = taille.replace(",", ".");  //remplace les virgule par des points de la case Taille pour eviter les erreurs

        poids = Number(poids);      //Tansforme le contenu en Number 
        taille = Number(taille);    //Tansforme le contenu en Number

        if (isNaN(poids) == true || isNaN(taille) == true) {    //si la case taille ou la case poids contient une valeur qui n'est pas un caractère numerique
            messageerror = "Veuillez verifier que les caractères entrés sont des caractères numeriques"; //créer une alert qui leur demande de changer leur valeures
            alert(messageerror);    //Affiche le message d'erreur

        } else {        //Sinon calculer l'imc

            let imc = 0; //variable allant contenir le resultat de la fonction calculerimc
            let imctofixed = 0; //variable allant contenir le resultat de l'imc avec un chiffre apres la virgule
            imc = calculerIMC(poids, taille);  //appelle la fonction calculerimc
            imctofixed = imc.toFixed(1);        //fixe la virgule a un chiffre apres la virgule
            let interprete = interpreterIMC(imctofixed);
            $("#textIMC").html(imctofixed + " " + interprete) ;     //modifie le texte afin d'afficher l'imc
        }

    });




    function calculerIMC(prmPoids, prmTaille) {         //fonction calculerim avec 2 parametre taille et poids
        let valRetour = prmPoids / (prmTaille * prmTaille);  //calcul l'imc 
        return valRetour;                               //renvoie la valeur de l'imc
    }   
    
    
    

    function interpreterIMC(prmIMC) {       //focntion interpreterIMC qui dit en fonction de l'imc l'etat du poids
        let resultinter = "";

        if (prmIMC < 16.5) {
            resultinter = "Dénutrition";
        }
        else if ((prmIMC >= 16.5) && (prmIMC < 18.5)) {
            resultinter = "Maigreur";
        }

        else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
            resultinter = "Corpulence Normale";
        }

        else if ((prmIMC >= 25) && (prmIMC < 30)) {
            resultinter = "Surpoids";
        }
        else if ((prmIMC >= 30) && (prmIMC < 35)) {
            resultinter = "Obesité Modéré";
        }

        else if ((prmIMC >= 35) && (prmIMC < 40)) {
            resultinter = "Obesité Sévère";
        }

        else if (prmIMC >= 40) {
            resultinter = "Obesité Morbide";
        }

        return resultinter ;
    }






});

