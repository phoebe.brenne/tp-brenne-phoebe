/*
BRENNE Phoebé
Créer le 26/03

Application avec une fonction Alphabet qui renvoit l'alphabet majuscule dans une alert
*/

let message = Alphabet() ;  //message recuperant la fonction alphabet
alert(message) ;            //affiche le resultat de la fonction alphabet




function Alphabet(){        //fonction alphabet
       
let alpha = "" ;            //message de retour

for(let i = 65; i <= 90 ; i++){     //boucle qui cherche les majuscule
    let lettre ;
    lettre = String.fromCharCode(i) ;   //retranscription du code hexa en lettre 
    alpha = alpha + lettre ;            //incrementation de la lettre au message de retour

}


return alpha ;      //valeur de retour qui retourne l'alphabet
}